import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'geocoding', loadChildren: () => import('./pages/geocoding/geocoding.module').then(m => m.GeocodingModule) },
  { path: 'taxi-ride', loadChildren: () => import('./pages/taxi-ride/taxi-ride.module').then(m => m.TaxiRideModule) },
  { path: 'directions', loadChildren: () => import('./pages/directions/directions.module').then(m => m.DirectionsModule) },
  
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
