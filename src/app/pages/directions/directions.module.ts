import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule } from '@angular/forms';

import { DirectionsRoutingModule } from './directions-routing.module';
import { DirectionsFormComponent } from './directions-form/directions-form.component';


@NgModule({
  declarations: [
    DirectionsFormComponent
  ],
  imports: [
    CommonModule,
    DirectionsRoutingModule,
    FormsModule
  ]
})
export class DirectionsModule { }
