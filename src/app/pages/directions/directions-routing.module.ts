import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DirectionsFormComponent } from './directions-form/directions-form.component';

const routes: Routes = [
  { path: '', component: DirectionsFormComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DirectionsRoutingModule { }
