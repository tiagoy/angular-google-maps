import { Component, OnInit, NgZone } from '@angular/core';

declare var google: any;

@Component({
  selector: 'app-directions-form',
  templateUrl: './directions-form.component.html',
  styleUrls: ['./directions-form.component.css']
})
export class DirectionsFormComponent implements OnInit {

  lat: string = '';
  lng: string = '';

  directionsService = new google.maps.DirectionsService();
  directionsDisplay = new google.maps.DirectionsRenderer();
  map: any;
  startPosition: any;
  originPosition?: string;
  destinationPosition?: string;

  departure: string = '';
  arrival: string = '';
  search: string = '';
  googleAutocomplete = new google.maps.places.AutocompleteService();
  searchResults = new Array<any>();

  title = ''

  constructor(private ngZone: NgZone) { }

  ngOnInit() {
    this.getCurrentLocation();
  }

  checkPin($event: KeyboardEvent) {
    console.log($event)
    console.log($event.isTrusted)
  }

  initializeMap(lat: any, long: any) {
    let position = new google.maps.LatLng(lat, long);

    const mapOptions = {
      zoom: 19,
      center: position,
      disableDefaultUI: true
    }

    this.map = new google.maps.Map(document.getElementById('map'), mapOptions);
    this.directionsDisplay.setMap(this.map);

    const marker = new google.maps.Marker({
      position: position,
      map: this.map,
    });
  }

  getCurrentLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(position => {
        let lat = position.coords.latitude;
        let lng = position.coords.longitude;
        this.initializeMap(lat, lng)
        console.log(lat)
        console.log(this.lat)
        localStorage.setItem('lat', lat.toString());
        localStorage.setItem('lng', lng.toString());
      });
    }
    else {
      alert("Geolocation is not supported by this browser.");
    }
  }

  searchOrigin() {
    console.log(this.departure)
    let departure = this.departure
    console.log(departure)
    this.searchChanged(this.departure)

    
  }


  selectOrigin(xxx: any) {
    console.log('departure')
    console.log(xxx.description)
    this.departure = xxx.description;
    this.title = xxx.description;
    

    this.searchChanged(xxx.description)
  }

  searchDestin() {
    return this.searchChanged(this.arrival)
  }

  searchChanged(search: string) {
    console.log(this.search)
    if (!search.trim().length) return;
    console.log(this.search)

    let lat = localStorage.getItem('lat');
    let lng = localStorage.getItem('lng');

    const center = { lat: Number(lat), lng: Number(lng) };

    const defaultBounds = {
      north: center.lat + 0.1,
      south: center.lat - 0.1,
      east: center.lng + 0.1,
      west: center.lng - 0.1,
    };

    this.googleAutocomplete.getPlacePredictions({
      bounds: defaultBounds,
      input: search
    },
      (predictions: any) => {
        //console.log(predictions)
        //console.log(predictions.description)
        this.ngZone.run(() => {
          this.searchResults = predictions;
        })

      })
  }


  calculateRoute(item: any) {
    this.search = '';
    console.log(item.description)
    this.geocoder(item.description)
    
  }

  happy(destLat: any, destLng: any){
    
    let lat = localStorage.getItem('lat');
    let lng = localStorage.getItem('lng');

    let originPosition = new google.maps.LatLng(Number(lat), Number(lng));
    let destinPosition = new google.maps.LatLng(destLat, destLng);

    this.getDistancia(originPosition, destinPosition)

    const request = {
      // Pode ser uma coordenada (LatLng), uma string ou um lugar
      origin: originPosition,
      destination: destinPosition,
      travelMode: 'DRIVING'
    };

    this.traceRoute(this.directionsService, this.directionsDisplay, request);
  }

  traceRoute(service: any, display: any, request: any) {
    service.route(request, function (result: any, status: string) {
      if (status == 'OK') {
        display.setDirections(result);
      }
    });
  }
  

  pegaVariavel(destLat: any, destLng: any) {
    this.happy(destLat, destLng)
  }

  geocoder(address: any) {
  
  var geocoder = new google.maps.Geocoder();

    geocoder.geocode({ 'address': address }, (results: any, status: any) => {
      if (status == google.maps.GeocoderStatus.OK) {
        let destLat = results[0].geometry.location.lat()
        let destLng = results[0].geometry.location.lng();
        this.pegaVariavel(destLat, destLng)
        
      } else {
        alert('Geocode was not successful for the following reason: ' + status);
      }     
    }); 
  }

  getDistancia(origen: string, destino: string) {
    return new google.maps.DistanceMatrixService().getDistanceMatrix({'origins': [origen], 'destinations': [destino], travelMode: 'DRIVING'}, (results: any) => {
        console.log('resultados distancia (mts) -- ', results.rows[0].elements[0].distance.value)
    });
  }
}
