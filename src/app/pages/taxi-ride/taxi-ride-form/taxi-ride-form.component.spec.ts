import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxiRideFormComponent } from './taxi-ride-form.component';

describe('TaxiRideFormComponent', () => {
  let component: TaxiRideFormComponent;
  let fixture: ComponentFixture<TaxiRideFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TaxiRideFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxiRideFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
