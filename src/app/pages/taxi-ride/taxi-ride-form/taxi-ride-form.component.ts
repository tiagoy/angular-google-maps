import { Component, OnInit, NgZone } from '@angular/core';

declare var google: any;

@Component({
  selector: 'app-taxi-ride-form',
  templateUrl: './taxi-ride-form.component.html',
  styleUrls: ['./taxi-ride-form.component.css']
})
export class TaxiRideFormComponent implements OnInit {

  lat: number = 0;
  long: number = 0;

  map: any;

  public search: string = '';
  private googleAutocomplete = new google.maps.places.AutocompleteService();
  public searchResults = new Array<any>();
  text = ''; //initialised the text variable

  constructor(private ngZone: NgZone) {
    console.log(google);
  }

  ngOnInit() {
    this.getCurrentLocation();
  }

  getCurrentLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(position => {
        this.lat = position.coords.latitude;
        this.long = position.coords.longitude;
        this.showMap(this.lat, this.long)
      });
    }
    else {
      alert("Geolocation is not supported by this browser.");
    }
  }

  showMap(lat: any, long: any) {

    console.log(lat, long)

    const position = new google.maps.LatLng(lat, long);

    const mapOptions = {
      zoom: 14,
      center: position,
      disableDefaultUI: true
    }

    this.map = new google.maps.Map(document.getElementById('map'), mapOptions);

    const marker = new google.maps.Marker({
      position: position,
      map: this.map,

      //Titulo
      title: 'Minha posição',

      //Animção
      animation: google.maps.Animation.DROP, // BOUNCE

      //Icone
      //icon: 'assets/imgs/pessoa.png'
    });
  }

  searchChanged() {
    if (!this.search.trim().length) return;

    const center = { lat: -16.6778971, lng: -49.2704630 };

    const defaultBounds = {
      north: center.lat + 0.1,
      south: center.lat - 0.1,
      east: center.lng + 0.1,
      west: center.lng - 0.1,
    };

    this.googleAutocomplete.getPlacePredictions({
      bounds: defaultBounds,
      input: this.search
    },
      (predictions: any) => {
        //console.log(predictions)
        //console.log(predictions.description)
        this.ngZone.run(() => {
          this.searchResults = predictions;
        })

      })
  }

  onKeyUp(x: any) { // appending the updated value to the variable
    this.text += x.target.value + ' | ';
  }

  calcRoute(item: any) {
    this.search = '';
    //console.log(item)
    console.log(item.description)

    var geocoder = new google.maps.Geocoder(item.description);

    const info : any = geocoder.geocode({ 'address': item.description }, function (results: any, status: any) {
      if (status == google.maps.GeocoderStatus.OK) {
        console.log(results[0].geometry.location.lat())
        console.log('catarrento')
        console.log(results[0].geometry.location.lng())
      } else {
        alert('Geocode was not successful for the following reason: ' + status);
      }
    });
  }

  

  
}
