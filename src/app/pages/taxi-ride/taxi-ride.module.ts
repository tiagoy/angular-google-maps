import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule } from '@angular/forms';

import { TaxiRideRoutingModule } from './taxi-ride-routing.module';
import { TaxiRideFormComponent } from './taxi-ride-form/taxi-ride-form.component';


@NgModule({
  declarations: [
    TaxiRideFormComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    TaxiRideRoutingModule
  ]
})
export class TaxiRideModule { }
