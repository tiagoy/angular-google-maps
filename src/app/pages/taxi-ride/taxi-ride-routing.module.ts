import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TaxiRideFormComponent } from './taxi-ride-form/taxi-ride-form.component';

const routes: Routes = [
  { path: '', component: TaxiRideFormComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TaxiRideRoutingModule { }
