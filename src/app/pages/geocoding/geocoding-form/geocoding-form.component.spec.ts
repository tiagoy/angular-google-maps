import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GeocodingFormComponent } from './geocoding-form.component';

describe('GeocodingFormComponent', () => {
  let component: GeocodingFormComponent;
  let fixture: ComponentFixture<GeocodingFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GeocodingFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GeocodingFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
