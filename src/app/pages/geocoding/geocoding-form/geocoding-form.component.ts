import { Component, OnInit } from '@angular/core';

declare var google: any;

@Component({
  selector: 'app-geocoding-form',
  templateUrl: './geocoding-form.component.html',
  styleUrls: ['./geocoding-form.component.css']
})
export class GeocodingFormComponent implements OnInit {
  directionsService = new google.maps.DirectionsService();
  directionsDisplay = new google.maps.DirectionsRenderer();
  map: any;
  startPosition: any;
  originPosition?: string;
  destinationPosition?: string;

  constructor() { }

  ngOnInit() {
    this.initializeMap();
  }

  initializeMap() {
    this.startPosition = new google.maps.LatLng(-21.763409, -43.349034);

    const mapOptions = {
      zoom: 18,
      center: this.startPosition,
      disableDefaultUI: true
    }

    this.map = new google.maps.Map(document.getElementById('map'), mapOptions);
    this.directionsDisplay.setMap(this.map);

    const marker = new google.maps.Marker({
      position: this.startPosition,
      map: this.map,
    });
  }

  calculateRoute() {
    console.log('teste')

    console.log('teste2')
    let originPosition = new google.maps.LatLng(-21.763409,-43.349034);
    let destinationPosition = new google.maps.LatLng(-16.822678,-49.247347);
    const request = {
      // Pode ser uma coordenada (LatLng), uma string ou um lugar
      origin: originPosition,
      destination: destinationPosition,
      travelMode: 'DRIVING'
    };

    this.traceRoute(this.directionsService, this.directionsDisplay, request);

  }

  traceRoute(service: any, display: any, request: any) {
    service.route(request, function (result: any, status: string) {
      if (status == 'OK') {
        display.setDirections(result);
      }
    });
  }


}
