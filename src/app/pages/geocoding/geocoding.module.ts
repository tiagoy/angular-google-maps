import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GeocodingRoutingModule } from './geocoding-routing.module';
import { GeocodingFormComponent } from './geocoding-form/geocoding-form.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    GeocodingFormComponent
  ],
  imports: [
    CommonModule,
    GeocodingRoutingModule,
    FormsModule
  ]
})
export class GeocodingModule { }
