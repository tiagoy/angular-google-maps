import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GeocodingFormComponent } from './geocoding-form/geocoding-form.component';

const routes: Routes = [
  { path: '', component: GeocodingFormComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GeocodingRoutingModule { }
